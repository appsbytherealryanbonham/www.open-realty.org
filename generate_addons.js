const fs = require('fs')
const yamlFront = require('yaml-front-matter')
const path = require('path')
const { versions } = require('process')

const docFolder = './docs/_addons/'
const homepage = 'https://www.open-realty.org'

let feed = {
    version: 'https://jsonfeed.org/version/1',
    user_comment: 'Open-Realty Addons',
    title: 'Open-Realty Official Addons',
    description: 'Open-Realty Official Addons List',
    home_page_url: homepage,
    feed_url: `${homepage}/addons.json`,
    author: {
        name: 'Ryan Bonham'
    },
    items: []
}

fs.readdirSync(docFolder).forEach(file => {

    let contents = fs.readFileSync(`${docFolder}${file}`, 'utf8')
    let yaml = yamlFront.loadFront(contents)
    let versions = []
    yaml.versions.forEach(version => {
        versions.push({
            version: version.version,
            date: version.date,
            stability: version.stability,
            download_url: version.download_url,
            min_compatibility: version.min_compatibility,
            max_compatibility: version.max_compatibility
        })
    })
    //Make sure we have the versions sorted by date.
    versions.sort(function (a, b) {
        var keyA = new Date(a.date),
            keyB = new Date(b.date);
        // Compare the 2 dates
        if (keyA < keyB) return 1;
        if (keyA > keyB) return -1;
        return 0;
    });
    /*
    ---
  title: TransparentRETS
  description: |
    TransparentRETS™ is a import tool, designed to import raw RETS listing data and photos from a Multiple Listing Service (MLS) RETS server directly into a Open-Realty listing database.
  versions:
    - version: 2.3.0-beta.1
      date: 2021-02-08
      stability: prerelease
      download_url: https://gitlab.com/appsbytherealryanbonham/transparentrets/-/package_files/7002153/download
  folder: transaprentRETS
  url: https://gitlab.com/appsbytherealryanbonham/transparentrets
  docs: https://docs.open-realty.org/nav.addons/01.transaprentRETS/
  */

    feed.items.push({
        title: yaml.title,
        author: yaml.author,
        description: yaml.description,
        homepage: yaml.url,
        docs: yaml.docs,
        folder: yaml.folder,
        versions: versions
    })

})
feed.items.reverse()
fs.writeFileSync('./public/addons.json', JSON.stringify(feed, null, 2));