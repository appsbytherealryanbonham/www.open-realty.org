---
title: Open-Realty Features
date: 2021-02-08
description:
  Listing Editor, VTour Support, Blog, WYSIWYG Page Editor, Menu Editor, Lead Management, and more
---
 
# Features of Open-Realty
 
Open-Realty is a feature rich application, supporting listing management with customizable fields, blogging, wysiwyg page editing, lead management, and more...


## Listing Editor

Listing Editor with support for attaching images, files, and vtours. 

<img src="/assets/img/features/listing_editor.png" />
 

## Blog Manager

Rich Blogging Platform, with tag and category support. With our Wordpress import you can manage your blog and listing in one place.

<img src="/assets/img/features/blog_editor.png" />
 
 
 ## Lead Manager

Track leads with automatic assignment to agents or office managers.

<img src="/assets/img/features/leadmanager.png" />