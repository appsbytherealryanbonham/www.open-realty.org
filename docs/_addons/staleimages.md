---
title: StaleImages
author: Ryan Bonham
description: |
  StaleImages is an add-on for Open-Realty® that find and removes images either missing in your database or filesystem.
versions:
  - version: 1.1.1
    date: 2022-02-03
    stability: stable
    download_url: https://gitlab.com/appsbytherealryanbonham/staleimages/-/package_files/28093548/download
    min_compatibility: 3.4.0
    max_compatibility:
  - version: 1.1.0
    date: 2022-02-03
    stability: stable
    download_url: https://gitlab.com/appsbytherealryanbonham/staleimages/-/package_files/28083024/download
    min_compatibility: 3.4.0
    max_compatibility:
folder: staleimages
url: https://gitlab.com/appsbytherealryanbonham/staleimages
docs: 
---

StaleImages is an add-on for Open-Realty® that find and removes images either missing in your database or filesystem.
