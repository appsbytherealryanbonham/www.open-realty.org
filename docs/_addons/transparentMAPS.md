---
title: TransparentMAPS
author: Ryan Bonham
description: |
  TransparentMAPS™ is an add-on for Open-Realty® that uses the Google Maps API to embed maps into its various pages to display the locations of listings.
versions:
  - version: 3.4.0
    date: 2022-08-13
    stability: stable
    download_url: https://gitlab.com/appsbytherealryanbonham/transparentmaps/-/package_files/49271361/download
    min_compatibility: 3.6.0-beta.1
    max_compatibility:
  - version: 3.4.0-beta.1
    date: 2022-07-06
    stability: prerelease
    download_url: https://gitlab.com/appsbytherealryanbonham/transparentmaps/-/package_files/44995990/download
    min_compatibility: 3.6.0-beta.1
    max_compatibility:
  - version: 3.3.2
    date: 2021-11-30
    stability: stable
    download_url: https://gitlab.com/appsbytherealryanbonham/transparentmaps/-/package_files/22855085/download
    min_compatibility: 3.4.2
    max_compatibility: 3.6.0
  - version: 3.3.1
    date: 2021-11-22
    stability: stable
    download_url: https://gitlab.com/appsbytherealryanbonham/transparentmaps/-/package_files/22170865/download
    min_compatibility: 3.4.2
    max_compatibility: 3.6.0
  - version: 3.3.0
    date: 2021-02-27
    stability: stable
    download_url: https://gitlab.com/appsbytherealryanbonham/transparentmaps/-/package_files/7612872/download
    min_compatibility: 3.4.0-beta.1
    max_compatibility: 3.4.1
  - version: 3.3.0-beta.1
    date: 2021-02-20
    stability: prerelease
    download_url: https://gitlab.com/appsbytherealryanbonham/transparentmaps/-/package_files/7355697/download
    min_compatibility: 3.4.0-beta.1
    max_compatibility: 3.4.1
folder: transparentmaps
url: https://gitlab.com/appsbytherealryanbonham/transparentmaps
docs: https://docs.open-realty.org/nav.addons/02.transparentMAPS/
---

TransparentMAPS™ is an add-on for Open-Realty® that uses the Google Maps API to embed maps into its various pages to display the locations of listings.
