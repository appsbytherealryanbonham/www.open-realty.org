---
title: TransparentRETS
author: Ryan Bonham
description: |
  TransparentRETS™ is an import tool; designed to import raw RETS listing data and photos from a Multiple Listing Service (MLS) RETS server directly into an Open-Realty listing database.
versions:
  - version: 2.4.0
    date: 2022-08-13
    stability: stable
    download_url: https://gitlab.com/appsbytherealryanbonham/transparentrets/-/package_files/49271336/download
    min_compatibility: 3.6.0-beta.1
    max_compatibility:
  - version: 2.4.0-beta.1
    date: 2022-07-06
    stability: prerelease
    download_url: https://gitlab.com/appsbytherealryanbonham/transparentrets/-/package_files/44995777/download
    min_compatibility: 3.6.0-beta.1
    max_compatibility:
  - version: 2.3.2
    date: 2021-11-25
    stability: stable
    download_url: https://gitlab.com/appsbytherealryanbonham/transparentrets/-/package_files/22482004/download
    min_compatibility: 3.4.2
    max_compatibility: 3.6.0
  - version: 2.3.1
    date: 2021-11-22
    stability: stable
    download_url: https://gitlab.com/appsbytherealryanbonham/transparentrets/-/package_files/22170798/download
    min_compatibility: 3.4.2
    max_compatibility: 3.6.0
  - version: 2.3.0
    date: 2021-02-26
    stability: stable
    download_url: https://gitlab.com/appsbytherealryanbonham/transparentrets/-/package_files/7605355/download
    min_compatibility: 3.4.0-beta.1
    max_compatibility: 3.4.1
  - version: 2.3.0-beta.2
    date: 2021-02-21
    stability: prerelease
    download_url: https://gitlab.com/appsbytherealryanbonham/transparentrets/-/package_files/7365639/download
    min_compatibility: 3.4.0-beta.1
    max_compatibility: 3.4.1
  - version: 2.3.0-beta.1
    date: 2021-02-08
    stability: prerelease
    download_url: https://gitlab.com/appsbytherealryanbonham/transparentrets/-/package_files/7002153/download
    min_compatibility: 3.4.0-beta.1
    max_compatibility: 3.4.1
folder: transparentRETS
url: https://gitlab.com/appsbytherealryanbonham/transparentrets
docs: https://docs.open-realty.org/nav.addons/01.transaprentRETS/
---

TransparentRETS™ is an import tool; designed to import raw RETS listing data and photos from a Multiple Listing Service (MLS) RETS server directly into an Open-Realty listing database.
