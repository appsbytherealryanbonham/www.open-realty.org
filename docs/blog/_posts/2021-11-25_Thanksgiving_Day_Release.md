---
title: Thanksgiving Day Releases - Open-Realty v3.4.3, TransparentRETS v2.3.2
date: 2021-11-25
description: Open-Realty v3.4.3, TransparentRETS v2.3.2
---

# Thanksgiving Day Releases - Open-Realty v3.4.3, TransparentRETS v2.3.2

This Open-Realty release contains a security patch for moment.js that we use. Also some more PHP 8 fixes, and agent vcard support.

# Open-Realty Changes

## [3.4.3] - 2021-11-25

Security Fix for moment.js, vcard support, more PHP8 fixes

### Fixed

- Agent vcard download reenabled
- moment.js - SECURITY FIX
- Fix Blog Count
- PHP 8 / ADODB Fixes.

## Changed

- Javascript Library Update
  - Jquery 3.6
  - moment.js
  - fullcandar v3
- Updated php-cs-fixer
- Updated docker-compose.yml to support testing mysql8 and mariadb 10.7

# TransparentRETS Changes

## [2.3.2] - 2021-11-25

### Fixed

- More PHP 8 ADODB Fixes.

### Changed

- Update php-cs-fixer to latest version

# Download Information

- [Open-Realty Download Here](https://gitlab.com/appsbytherealryanbonham/open-realty/-/releases)
- [TransparentRETS Download Here](https://gitlab.com/appsbytherealryanbonham/transparentrets/-/releases)

Make sure to select the Package Download, as highlighted in this example screenshot.

<img src="/assets/blog/release_download_screenshot.png" />
