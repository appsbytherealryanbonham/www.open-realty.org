---
title: Open-Realty 3.5.1 - So Many Updates
date: 2021-12-20
description: This release contains bug fixes, and more third party updates.
---

# Open-Realty 3.5.1 - So Many Updates

Building on top of the reset 3.5.0 release, this release fixes a couple of bugs that were discovered, and continues out path to update all our dependencies.

## [v3.5.1] - 2021-12-20

### Fixes / Changes

- Blog & Page Editors Not Loading
- Improvement to pingback registration, including fixed bug where registration failed if you were not using SEO Urls
- Fix MagicParser for SEO URls
- Fix a bunch of code bugs found via intellisense.
- Update phpxmlrpc to v4.
- Upgrade browscap-php to v6
- Update Timezone list

# Download Information

- [Open-Realty Download Here](https://gitlab.com/appsbytherealryanbonham/open-realty/-/releases)

Make sure to select the Package Download, as highlighted in this example screenshot.

<img src="/assets/blog/release_download_screenshot.png" />
