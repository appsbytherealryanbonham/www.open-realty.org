---
title: First Betas for TransparentRETS and TransparentMaps for Open-Realty 3.6.0
date: 2022-07-06
description: First Betas for TransparentRETS and TransparentMaps for Open-Realty 3.6.0
---

The first beta releases of TransparentRETS and TransparentMaps for Open-Realty 3.6.0 are ready. These is NOT a production-ready release. The big change in this release is the update the addons to work with the new Open-Realty admin ui. The addons should be functional, but still have some more cosmetic UI updates to get them to completely fit the new look and feel.

-

# Download Information

- [TransparentRETS 2.4.0-beta.1 Download Here](https://gitlab.com/appsbytherealryanbonham/transparentrets/-/releases/2.4.0-beta.1)
- [TransparentMaps 3.4.0-beta.1 Download Here](https://gitlab.com/appsbytherealryanbonham/transparentmaps/-/releases/3.4.0-beta.1)

Make sure to select the Package Download, as highlighted in this example screenshot.

<img src="/assets/blog/release_download_screenshot.png" />
