---
title: TransparentMaps 3.3.2 - Google Geocoder Fix
date: 2021-11-25
description: This release fixes an issue with the Google Geocoder not working.
---

# TransparentMaps 3.3.2 - Google Geocoder Fix

This release fixes an issue with the Google Geocoder not working.

## [v3.3.2] - 2021-11-30

### Fixed

- Fix Google Geocoder

### Changed

- Add better output on geocode failures.

# Download Information

- [TransparentMaps Download Here](https://gitlab.com/appsbytherealryanbonham/transparentmaps/-/releases)

Make sure to select the Package Download, as highlighted in this example screenshot.

<img src="/assets/blog/release_download_screenshot.png" />
