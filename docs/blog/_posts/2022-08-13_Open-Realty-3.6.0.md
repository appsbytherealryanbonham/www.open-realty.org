---
title: Open-Realty 3.6.0 Released
date: 2022-08-13
description: Open-Realty 3.6.0 - New Admin Template
---

Open-Realty 3.6.0 is a major step forward for Open-Realty. The new Admin UI, is built using bootstrap 5 and is fully responsive. We have reduced teh number of jquery plugins we use. Made a TON of security improvements to Open-Realty. Hopefully this release excites you as much as it does us.

## [3.6.0] - 2022-08-13

### Security

- Fix CVE-2022-31129 Upgrade Moment JS, Inefficient Regular Expression Complexity
- Improve DAST Scanning
- Improve SAST Scanning (Disable some PHPCS rules)
- Added Anti-CSRF protection on edit_listings form.
- Added Anti-CSRF protection on media upload form.
- Added Anti-CSRF protection on media edit form.
- Security Patch Jquery UI 1.13.2 - CVE-2022-31160
- Added Anti-CSRF protection on edit page form.
- Added Anti-CSRF protection on edit_user form.
- Added Anti-CSRF protection on email a friend form.
- Added Anti-CSRF protection on add page form.
- Added Anti-CSRF protection on insert property class form.
- Added Anti-CSRF protection on site config forms.
- Added Anti-CSRF protection on add blog form.
- Added Anti-CSRF protection on the blog_wpinject form.
- Added Anti-CSRF protection on the add_blog_category_form form.
- Added Anti-CSRF protection on the add_blog_tag_form form.
- Added Anti-CSRF protection on the send_forgot form.
- Added Anti-CSRF protection on the edit_blog_tag_form form.
- Added Anti-CSRF protection on the edit_blog_category_form form.
- Added Anti-CSRF protection on the menu_selection_form form.
- Added Anti-CSRF protection on the add_menu_form form.
- Added Anti-CSRF protection on the add_item_form form.
- Fix Code Injection Warning in FileManager
- Added Anti-CSRF protection on the ajax_save_user_rank call.
- Added Anti-CSRF protection on the modify_pclass_form form.
- Added Anti-CSRF protection on the site_config_tracking form.
- Added Anti-CSRF protection on the addon_manager manual upload form.
- Added Anti-CSRF protection on the edit listing quick filter forms.
- Added Anti-CSRF protection on the user manager quick filter forms.

### Changed

- Make js look for class copyright_year instead of ID, when inserting current year.
- Upgrade abraham/twitteroauth to v4.
- Fix SameSite setting on php session cookie, that broke twitter auth.
- Fix pagination on edit_listings, so it returns 403 access denied if you exceed max cur_page
- Fix pagination on user_manager, so it returns 403 access denied if you exceed max cur_page
- Misc Yarn/Composer Updates
- Improve .htaccess and admin/.htaccess
- Fix pagination on edit_listings & user_manager to handle cur_page < 0
- Removed some dead code from ckeditor filemanager.
- Fixed duplicate JS calls on page editor, resulting in duplicate saves..
- Removed Jquery Cookie library, which we no longer use.
- Load JQuery on popup and printer friendly pages.
- Fixed handling of wpinjectform and removed use of ajaxForm plugin
- Fix switching menus in menu editor, selection didn't work after initial menu.
- Fixed pagination but on edit listing and user manager when using filters.
- Fixed wpinject php errors.
- Remove use of ajaxform in media_upload and ckeditor filemanager.
- Remove jquery form plugin

### Template

- admin/template/default/add_lead.html - Remove reference to {template_url}/images/ajax-loader.gif
- Add missing blog_edit_comments.html template

## [3.6.0-beta.1] - 2022-07-05

### Fixed

- Updated Composer Install
- Update Composer Installer to handle composer version upgrades without breaking CI
- Update Security Scanners for Gitlab 15.
- Update dependencies
- Enable load_js and load_js_last for admin, as we have addons that still use it.
- Remove uneeded js from blog_editor.
- The controlpanel_template field in site config, was readonly.
- Fix duplicate DOM Ids on controlpanel form.
- Improve autocomplete on login form.
- Remove deprecated call to jqueryUI accordion() in lead editor.
- Remove document.write call to clear Chrome warning.
- Add CSP Headers for admin area to help improve security.
- Removed console.dir() debug logs.
- Fixed Generic Object Injection Sink vulnerability in lead editor.
- Fix height of vertical navbar
- Fix highlight of active page on vertical navbar.
- Fix package command, that was not compiling CSS..

### Languages Updates

- Add descriptions for lat/long fields.

## [3.6.0-alpha.2] - 2022-04-07

### Fixed

- Login Reset Form showed a SQL error, and always reported that reset link was invalid.
- Login form now displays forgot password form. [#110])(https://gitlab.com/appsbytherealryanbonham/open-realty/-/issues/110_)
- Add a check_allow_agent_signup tag for permissions checks.
- Address Blog/Page autosave issues. [#112](https://gitlab.com/appsbytherealryanbonham/open-realty/-/issues/112)
- Update Addon Manager to use ZipArchive function instead of zip_open..
- Fix tabs on addon_manager
- Update OneClickUpgrade to use ZipArchive function instead of zip_open..
- Removed support of "RSXM" format remote API. This removes usages of mcrype in api. Added "RSXM2" which is same format as "RSXM" without the built in encryption. Remove API should only be used on sites that use HTTPS to prevent secrets from being passed in clear.
- Bump some dependencies with minor updates.
- Add missing popup.html template files. [#87](https://gitlab.com/appsbytherealryanbonham/open-realty/-/issues/87)
- Fix handling of 'notfound' in magicuri parsing, to return admin index page.
- Fix Undefined Languages on site config social.
- Fix DAST Scanning
- Address Generic Object Injection Sink vulnerability in template editor
- Fix ESLint rule ID security/detect-non-literal-regexp in filemanager.js
- Update Dependencies
- Remove wysiwyg_execute_php setting, which was deprecated.
- Remove apikey setting, which is not longer used.
- Remove vtour_fovcontrolpanel_vtour_fov
- Fix issue saving controlpanel_search_list_separator
- Fix issue saving controlpanel fields that contained HTML

## [3.6.0-alpha.1] - 2022-03-12

### Changed

- New Bootstrap5 Admin Template Based on [Material DashBoard by Creative Tim](https://github.com/creativetimofficial/material-dashboard)
- Removed usage of ORBetterSerialze JQuery Plugin
- Removed usage of Jquery UI in the admin area.
- Removed usage of Jquery Validation Plugin in the admin area.
- forms.inc.php now outputs Bootstrap styled forms
- Removed cms_admin_integration template.
- Added `{check_action_(.*?)}` and `{!check_action_(.*?)}` tags. This will let you display/hide content in a template based on the OR action being performed. Eg `{check_action_index}I am an index{/check_action_index}` will show `I am an index` if the index page is being loaded. Useful for controlling CSS, etc in menus.

### Languages Updates

- We are now managing language translations using Crowdin. Anyone interested in helping proofread translations can signup at https://translate.open-realty.org/
- There were many new language variables added as part of the template work. The goal is to have 100% language coverage of the admin area for the 3.6.0 release.
- Spanish, Brazilian Portuguese, and Portuguese languages now ship with 3.6.0-alpha.1

### Template Changes

- **All Admin Templates Files (Old Templates will not work)**
-

# Download Information

- [Open-Realty Download Here](https://gitlab.com/appsbytherealryanbonham/open-realty/-/releases)

Make sure to select the Package Download, as highlighted in this example screenshot.

<img src="/assets/blog/release_download_screenshot.png" />
