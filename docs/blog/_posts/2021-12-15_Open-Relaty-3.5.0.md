---
title: Open-Realty 3.5.0 - So Many Updates
date: 2021-12-15
description: This release contains some long overdue updates to the third party libraries we use as well as security fixes.
---

# Open-Realty 3.5.0 - So Many Updates

The focus on 3.5.0 is updating open-realty's dependencies, improving security, and bug fixes. All users are strongly recommended to upgrade. Please note that third party templates will likely need to be upgraded/adjusted as part of this due to the dependency upgrades.

## [v3.5.0] - 2021-12-15

### Fixes / Changes

- SECUIRTY PATCH: Fixed a file exposure bug in CKEditor Filemanager plugin.
- There were a number of other security bugs fixed in the js library upgrades etc..
- A number of possible undefined var errors
- Cookies are not http only, improving security
- Enabled Security Scanning for CI
- Fixed menu editor dropdown for Blog / Page selection not working
- Fix colorbox that broke with latest jquery version.
- Update php-cs-fixer to format the CKEditor filemanager plugin
- Removed Old Jquery Versions
  - Remove Jquery 1.7.1
  - Remove jquery-1.2.6 from CKEditor filemanager plugin
- Removed JQuery Tools - This Change the Slideshow, Featured Listings, and Listing Statistics templates.
- Moved CKEditor installation from composer to yarn.
- Moved Third Party JS Packages to YARN and updated versions
  - superfish.js
  - jquery.uniform
  - ckeditor-youtube-plugin
  - ckeditor-quicktable-plugin
  - ckeditor4
  - jquery.splitter
  - jqueryfiletree
  - jstree
  - tablesorter
  - jquery.impromptu
  - jquery-filedrop
  - lightSlider
- PHP Library Changes
  - Replace aferrandini/phpqrcode library with endroid/qr-code library as phpqrcode was no longer maintained.
  - Replaced dapphp/securimage with gregwar/captcha as securimage was no longer maintained.
- Removed js-tree package that was not being used.
- Update CKEditor Plugins
  - YouTube
  - TableResizer
  - QuickTable
  - Open-Realty Filemanager
- CkEditor Filemanager
  - Fix bug in root folder protection
  - Fix Generic Object Injection Sink warnings
- Log Viewer
  - Remove Dead Code
  - Fix Generic Object Injection Sink warnings

### Template Changes

- admin/template/OR_small/main.html
- admin/template/default/addon_manager.html
- admin/template/default/blog_edit_index.html
- admin/template/default/listing_template_editor.css
- admin/template/default/menu_editor.css
- admin/template/default/menu_editor_index.html
- admin/template/default/site_config_general.html
- template/default/listing_detail_slideshow.html
- template/default/saved_searches.html
- template/default/style_default.css
- template/html5/admin_bar.css
- template/html5/featured_listing_horizontal.html
- template/html5/lib/MIT-LICENSE.txt (DELETED)
- template/html5/lib/jquery.uniform.min.js (DELETED)
- template/html5/lib/superfish.js (DELETED)
- template/html5/listing_detail_default.html
- template/html5/main.html
- template/html5/popup.html
- template/html5/style.css
- template/lazuli/main.html
- template/lazuli/popup.html
- template/mobile/main.html
- template/mobile/page1_main.html

# Download Information

- [Open-Realty Download Here](https://gitlab.com/appsbytherealryanbonham/open-realty/-/releases)

Make sure to select the Package Download, as highlighted in this example screenshot.

<img src="/assets/blog/release_download_screenshot.png" />
