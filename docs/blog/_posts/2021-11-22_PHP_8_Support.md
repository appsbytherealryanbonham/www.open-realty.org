---
title: PHP 8+ Support - Open-Realty v3.4.2, TransparentMaps v3.3.1, TransparentRETS v2.3.1
date: 2021-11-22
description: Open-Realty v3.4.2, TransparentMaps v3.3.1, TransparentRETS v2.3.1 have been released with PHP 8.0+ support
---

# Open-Realty v3.4.2, TransparentMaps v3.3.1, TransparentRETS v2.3.1 Released

PHP 8 is now supported with the latest version of Open-Realty, TransparentRETS, and TransparentMaps

# Open-Realty Changes

## [3.4.2] - 2021-11-21

Initial Support For PHP 8.

### Fixed

- Fixed numerous PHP 8 warnings
- Fixed Banned IP/Domain Detection on PHP 8
- Fixed Bug in Listing Field Create that required a Search Type Set
- Many other small bug fixes related to the PHP Warning Fixes.

### Template Changes

- admin/template/default/site_config_users.html Changed {lang_agent_default_canChangeExpirations_desc} to {lang_agent_default_canchangeexpirations_desc}
- admin/template/default/view_logs.html Changed {lang_log_viewer\_} to {lang_log_viewer}

# TransparentMaps Changes

## [v3.3.1] - 2021-11-22

### Changed

- Supports PHP 8.0+

# TransparentRETS Changes

## [2.3.1] - 2021-11-21

### Added

- PHP 8.0 Support

# Download Information

- [Open-Realty Download Here](https://gitlab.com/appsbytherealryanbonham/open-realty/-/releases)
- [TransparentRETS Download Here](https://gitlab.com/appsbytherealryanbonham/transparentrets/-/releases)
- [TransarentMaps Download Here](https://gitlab.com/appsbytherealryanbonham/transparentmaps/-/releases)

Make sure to select the Package Download, as highlighted in this example screenshot.

<img src="/assets/blog/release_download_screenshot.png" />
