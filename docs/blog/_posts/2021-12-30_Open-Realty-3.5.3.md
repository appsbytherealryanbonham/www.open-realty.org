---
title: Open-Realty 3.5.3 Released
date: 2021-12-30
description: Open-Realty 3.5.3 - More security improvements and bug fixes.
---

Open-Realty 3.5.3 is here just in time for new years, this release includes more security improvements and bug fixes. All users are strongly encouraged to upgrade.

### Fixed

- SECURITY: Fix File Path Traversal in Filemanager jqueryFileTree.php
- DEPRECATED: Ability to enable execution of PHP Code in WYSIWYG editor. This feature is BAD idea and is going away in 3.6.0. Use Addons instead of writing PHP in the WYSIWYG pages/blogs.
- Delete Item on menu editor was not working.
- Minor JS cleanups.
- Add CSRF Tokens to agent & member signup forms
- Added Xdebug config to docker-compose to support local debugging.
- Add CSRF Tokens to login forms.
- Added CSRF Tokens to Template Switcher
- Template switcher no longer support GET vars you must use POST
- Mobile Template Changer `{mobile_full_template_link}` only shows if allow_template_change is enabled.
- Add CSRF Tokens to contact agent form.
- Cleanup contact agent form code, and remove pro & free templates and replace with new default template.
- Fix SQL Errors in replace_search_field_tags()
- Fix JS for Flex, Lazuli, and Mobile Template
- Code review for phpcs warnings, and setting ignores where safe.
- Add CSRF Tokens to save search form
- Fix handling of duplicate saved searches
- Fix broken link when duplicate saved searches occur.

### Template Changes

- admin/template/default/login.html
- admin/template/default/menu_editor_index.html
- template/default/agent_signup.html
- template/default/contact_agent_default.html (Added)
- template/default/contact_agent_free.html (Deleted)
- template/default/contact_agent_pro.html (Deleted)
- template/default/login.html
- template/default/member_signup.html
- template/default/saved_searches_add.html
- template/flex.main.html
- template/html5/featured_listing_horizontal.html
- template/html5/listing_detail_default.html
- template/lazuli/main.html
- template/mobile/main.html
- template/mobile/page_1.html
-

### Language Changes

- Removed `$lang["incorrect_password"]`
- Removed `$lang["nonexistent_username"]`
- Updated `$lang["wysiwyg_execute_php_desc"]`
- Updated `$lang["allow_template_change_desc"]`
- Added `$lang["invalid_csrf_token"]`
- Added `$lang["signup_already_logged_in"]`
- Added `$lang["incorrect_username_password"]`

# Download Information

- [Open-Realty Download Here](https://gitlab.com/appsbytherealryanbonham/open-realty/-/releases)=

Make sure to select the Package Download, as highlighted in this example screenshot.

<img src="/assets/blog/release_download_screenshot.png" />
