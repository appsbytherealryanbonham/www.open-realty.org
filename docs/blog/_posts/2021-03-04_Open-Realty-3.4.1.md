---
title: Open-Realty 3.4.1 Released
date: 2021-03-04
description: Open-Realty 3.4.1 has been released with some important bug fixes..
---

# Open-Realty 3.4.1 Released

The release contains one template change for the admin area, and some other bug fixes/improvements. Everyone is encouraged to upgrade.

## Template Changes

- admin/template/default/blog_edit_index.html Javascript for Blog Index now part of template.

### Changed

- [#15](https://gitlab.com/appsbytherealryanbonham/open-realty/-/issues/15) get_latest_releases now caches response from server.
- [#16](https://gitlab.com/appsbytherealryanbonham/open-realty/-/issues/16) Improve Logging when get_url calls fail
- JQuery Form upgraded and now installed via yarn.

### Fixed

- [#14](https://gitlab.com/appsbytherealryanbonham/open-realty/-/issues/14) Autoupgrade was not correctly running db upgrades and bumping version number.
- [#17](https://gitlab.com/appsbytherealryanbonham/open-realty/-/issues/17) Do not override .htaccess during upgrade.

### Download Information

[Download Here](https://gitlab.com/appsbytherealryanbonham/open-realty/-/releases/3.4.1)

Make sure to select the Package Download, as highlighted in this example screenshot.

<img src="/assets/blog/release_download_screenshot.png" />
