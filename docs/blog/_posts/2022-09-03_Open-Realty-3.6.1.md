---
title: Open-Realty 3.6.1 Released
date: 2022-09-03
description: Open-Realty 3.6.1 - Bug fix for subdirectories & seo friendly urls.
---

Open-Realty 3.6.1 is a bug fix release, which addressed some issues discovered after the 3.6.0 release. This release should fix issues that users trying to run Open-Realty in a subdirectory of their site had with links/forms not working. This also fixes issues with the SEO friendly URLS in 3.6.0.

## [3.6.1] - 2022-09-03

### Changed

- Fix .htaccess RewriteCond that broke seo friendly urls.
- Fix some HTML/JS validation warning in the admin template.
- Fix undefined variable is listing\__update api call [#119](https://gitlab.com/appsbytherealryanbonham/open-realty/-/issues/119_)
- Fix baseurl tags in the material template.

### Misc

- Update gitpod settings, to have name & descriptions for ports.


# Download Information

- [Open-Realty Download Here](https://gitlab.com/appsbytherealryanbonham/open-realty/-/releases)

Make sure to select the Package Download, as highlighted in this example screenshot.

<img src="/assets/blog/release_download_screenshot.png" />
