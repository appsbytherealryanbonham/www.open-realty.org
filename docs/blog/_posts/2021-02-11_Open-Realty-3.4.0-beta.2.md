---
title: Open-Realty 3.4.0-beta.2 Released
date: 2021-02-11
description:
   Open-Realty 3.4.0-beta.2 Released
---
 
# Open-Realty 3.4.0-beta.2 Released
 
Our second beta for 3.4.0 is now available. This second beta fixes some install and packaging issues we discovered after the first beta release. 
Please note that Open-Realty now requires PHP 7.4, older PHP version will not work.
 
Check out the [change log for Open-Realty 3.4.0-beta.2](https://gitlab.com/appsbytherealryanbonham/open-realty/-/blob/main/CHANGELOG.md#340-beta2-2021-02-08) and start testing and contributing.

### Download Information

[Download Here](https://gitlab.com/appsbytherealryanbonham/open-realty/-/releases/3.4.0-beta.2)

Make sure to select the Package Download, as highlighted in this example screenshot. 

<img src="/assets/blog/release_download_screenshot.png" />
