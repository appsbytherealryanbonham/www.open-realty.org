---
title: TransparentMAPS first open source release is out!.
date: 2021-02-20
description: TransparentMAPS, starting with version 3.3.0-beta.1, goes open source and will be released under MIT license.
---

# TransparentMAPS goes Open Source

TransparentMAPS™ (TMAPS) is an add-on for Open-Realty® (OR) that uses the Google Maps API to embed maps into its various pages to display the locations of listings.

Today I am happy to announce, TransparentMAPS is now under the MIT open source license. All features from the commercial version 3.1.x have been carried over to the open source software.

Check out the [change log for TransparentMAPS 3.3.0-beta.1](https://gitlab.com/appsbytherealryanbonham/transparentmaps/-/blob/main/CHANGELOG.md#v230-beta1-2021-02-09) and start testing and contributing.

### Download Information

[Download Here](https://gitlab.com/appsbytherealryanbonham/transparentmaps/-/releases)

Make sure to select the Package Download, as highlighted in this example screenshot.

<img src="/assets/blog/release_download_screenshot.png" />
