---
title: Open-Realty 3.4.0-beta.4 Released
date: 2021-02-19
description: Open-Realty 3.4.0-beta.4 Released
---

# Open-Realty 3.4.0-beta.4 Released

Our fourth beta for 3.4.0 is now available. This fourth includes an update to the addon manager, and fixes for site statistics.

Please note that Open-Realty now requires PHP 7.4, older PHP version will not work.

Check out the [change log for Open-Realty 3.4.0-beta.4](https://gitlab.com/appsbytherealryanbonham/open-realty/-/blob/main/CHANGELOG.md#340-beta4-2021-02-19) and start testing and contributing.

### Download Information

[Download Here](https://gitlab.com/appsbytherealryanbonham/open-realty/-/releases/3.4.0-beta.4)

Make sure to select the Package Download, as highlighted in this example screenshot.

<img src="/assets/blog/release_download_screenshot.png" />
