---
title: Open-Realty 3.5.8 Released
date: 2022-02-05
description: Open-Realty 3.5.8 - Groundhog Day Bug Fixes
---

Open-Realty 3.5.8 we have improved the way we run cron jobs, fixed the twitter integration, and updated our vtour support.

## [3.5.8] - 2022-02-05

### Changed

- Enable ability to run cron jobs that require auth, via the CLI instead of running them via the web which requires CSRF Tokens.

  Instead of calling  
  `curl -d "user_name=ORADMINUSER&user_pass=ORADMINPASSWORD" http://www.yourdomain.com/admin/ajax.php?action=generate_sitemap`

  you can now call

  `php src/admin/index.php 'user_name=ORADMINUSER&user_pass=ORADMINPASSWORD' 'action=generate_sitemap'`

### Fixed

- Twitter Auth is working again.
- VTour Support is working again. Open-Realty now only supports using Equirectangular jpg for virtual tours. We no longer support the old .egg files. On Android, you can take "Photo Sphere" images to produce Equirectangular images. We use pannellum javascript to display images now.

# Download Information

- [Open-Realty Download Here](https://gitlab.com/appsbytherealryanbonham/open-realty/-/releases)

Make sure to select the Package Download, as highlighted in this example screenshot.

<img src="/assets/blog/release_download_screenshot.png" />
