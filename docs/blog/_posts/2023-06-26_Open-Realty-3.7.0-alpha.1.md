---
title: Open-Realty 3.7.0-alpha.1 Released
date: 2023-06-26
description: Open-Realty 3.7.0-alpha.1
---

Open-Realty 3.7.0-alpha.1 is now available. This release packs a ton of changes! 
This release, has focused on improving and modernizing the code base. Support for MariaDB is not improved, and no longer requires 32k page sizes.
Addons will require updated version to work with Open-Realty 3.7.0.

This is a development release, and should not be used on live sites.


## [3.7.0-alpha.1] - 2023-06-25

- PHP 8 is now minimal php version [#147](https://gitlab.com/appsbytherealryanbonham/open-realty/-/issues/147)
- Fix {pclass_link} returns relative url [#146](https://gitlab.com/appsbytherealryanbonham/open-realty/-/issues/146)
- Open-Realty Code Base is now namespaced. This will be a breaking change for addons, hooks, and any third party code
  that interacts with Open-Realty for it's API.
- PHP is now using strict_types and Psalm Error Level 4 issues are all resolved.
- Pslam Error Level 3 issues resolved
- Deprecation & Removal of usage of global $misc variable.
- Added BaseClass for all OpenRealty namespaced classes to use, to support Mocking.
- Resolve level 2 psalm issues.
- Add Settings Api
- Setup PDO database connection
- Add Integration Test for FieldsApi
- Add Unit Test for addons
- Phase 1 of removing Adodb complete. [#149](https://gitlab.com/appsbytherealryanbonham/open-realty/-/issues/149)
- Consolidate index.php, ajax.php, admin/index.php, admin/ajax.php, and pingback.php and handle with new Loader class.
- Update Composer dependencies, include psalm to get a fix for psalm #9066.
- Migrate FieldsAPI to use PDO
- Fix menu Editor Not Saving. [#157](https://gitlab.com/appsbytherealryanbonham/open-realty/-/issues/157)
- Fix Listing & User Date fields in API to expect ISO8601 formated dates. This matches HTML5 date field values.
- Add Integration Test for LogApi
- Improve Safety on Field Names in FieldsApi Create
- Convert ListingApi PDO
- Convert MediaApi to PDO and improve testing
- Covert MenuApi to PDO and add tests for MenuApi
- Covert PageApi to PDO and add tests for PageApi
- Fix codecoverage reports.
- Convert PClassApi to PDO and add tests.
- Convert UserApi to PDO and add tests.
- Update all Yarn and Composer Dependencies
- Split controlpanel table into multiple tables, to prevent need for 32k page sizes on MariaDB
- Fix Tooltips on fields that showed "help_outline" instead of the tooltip icon and text.
- Improve validation of agent email, mark it has html email field.
- Fix bug that save user_name as email when creating user.
- Fix bug in pclass parsing in Listing__search api.
- Make Settings API fail gracefully when loading additional settings table, to prevent failures durring upgrades.
- Fix Logs that logged wrong function name.
- Fix CI release process for alpha & beta releases, gnu grep.

# Download Information

- [Open-Realty Download Here](https://gitlab.com/appsbytherealryanbonham/open-realty/-/releases)

Make sure to select the Package Download, as highlighted in this example screenshot.

<img src="/assets/blog/release_download_screenshot.png" />
