---
title: Open-Realty 3.6.3 Released
date: 2022-10-08
description: Open-Realty 3.6.3 - Test, Test, and More Test
---

Open-Realty 3.6.3 is the last version of Open-Realty that will support PHP 7.4, the next release will required PHP 8.0+. This is primarily a bug fix release.
The one big change is that we have started adding better code testing to Open-Realty, this should lead to better quality releases in the future with less bugs as we build out out test suite. 

## [3.6.3] - 2022-10-08

### Changed
- This is the last release to support PHP 7.4. All future release will require PHP 8+
- Improved Display of Tabs on Edit Listing Template Field Dialog.
- Fixed display of * on required fields on listing, agent, and lead field editor screens.
- Fixed incorrect isplay of required status as no, even if it was yes on lead and listing field editor dialogs. [#139](https://gitlab.com/appsbytherealryanbonham/open-realty/-/issues/139)
- Fix php error in user__create api
- Fix Admin Site Config Forms now displaying template tags.
- Fix ability to collapse listing media widget image pane after initial load.
- Fix ability to collapse user media widget image pane after initial load.
  - Media widget now loads thumbnail images instead of main images and acceptance test added for this.. [#144] (https://gitlab.com/appsbytherealryanbonham/open-realty/-/issues/144)
- Google Auth was not calling set_session_vars() so login was failing to authorize user.
- Improve permission checks in listing__delete api
- Fix error handling in listing__delete api that caused api call to die() instead of returning an error.
- Fix error in uploading images in media__create api.
- Improve user__delete api, to remove userdb entry last, to ensure we do not orphan objects if errors occur.
  
### Security
- Install yarn updates, pull is security fix for node-sass
- Set autocomplete and spellcheck attributes on all password and user_name fields.
  
### Misc
- Start Adding some more unit tests
- Start adding User API Integration Test
- Start adding Acceptance/Browser Test
- Start Adding Listing API Integration tests
- Get Acceptance Test w/ code coverage working in CI.
- Add Acceptance test for media widget behavior in [#143](https://gitlab.com/appsbytherealryanbonham/open-realty/-/issues/143)
- Improve Setup/Teardown for Integration Test
- Add user media widget acceptance test and fix flaky test for media widget listing & user.
- Bundler should not package c3.php or tests for releases.
- Start adding unit test for Login, and standardize our test setups and documentation.
- Misc Code Documentation, Cleanup, adding unit tests.
- Start adding User API Integration tests.
- Improve Node Cache for Docker and CI


# Download Information

- [Open-Realty Download Here](https://gitlab.com/appsbytherealryanbonham/open-realty/-/releases)

Make sure to select the Package Download, as highlighted in this example screenshot.

<img src="/assets/blog/release_download_screenshot.png" />
