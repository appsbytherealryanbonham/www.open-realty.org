---
title: Open-Realty 3.6.0-alpha.1 Released
date: 2022-03-12
description: Open-Realty 3.6.0-alpha.1 - New Admin Template
---

This is our first developer release Open-Realty 3.6.0. This is NOT a production-ready release. This release is intended to let developers start working to update addons and help test our new admin template. The big change in this release is the new Admin Template, which is a bootstrap 5 template. I hope everyone likes the new design. Our next releases will be focusing on polishing and testing the template as well as some additional improvements we want to make to Open-Realty for the 3.6.0 release.

## [3.6.0-alpha.1] - 2022-03-12

### Changed

- New Bootstrap5 Admin Template Based on [Material DashBoard by Creative Tim](https://github.com/creativetimofficial/material-dashboard)
- Removed usage of ORBetterSerialze JQuery Plugin
- Removed usage of Jquery UI in the admin area.
- Removed usage of Jquery Validation Plugin in the admin area.
- forms.inc.php now outputs Bootstrap styled forms
- Removed cms_admin_integration template.
- Added `{check_action_(.*?)}` and `{!check_action_(.*?)}` tags. This will let you display/hide content in a template based on the OR action being performed. Eg `{check_action_index}I am an index{/check_action_index}` will show `I am an index` if the index page is being loaded. Useful for controlling CSS, etc in menus.

### Languages Updates

- We are now managing language translations using Crowdin. Anyone interested in helping proofread translations can signup at https://translate.open-realty.org/
- There were many new language variables added as part of the template work. The goal is to have 100% language coverage of the admin area for the 3.6.0 release.
- Spanish, Brazilian Portuguese, and Portuguese languages now ship with 3.6.0-alpha.1

### Template Changes

- **All Admin Templates Files (Old Templates will not work)**
-

# Download Information

- [Open-Realty Download Here](https://gitlab.com/appsbytherealryanbonham/open-realty/-/releases)

Make sure to select the Package Download, as highlighted in this example screenshot.

<img src="/assets/blog/release_download_screenshot.png" />
