---
title: Open-Realty Goes Open Source
date: 2021-02-08
description:
   Open-Realty, starting with version 3.4.0-beta.1, goes open source and will be released under MIT license.
---
 
# Open-Realty Goes Open Source
 
Open-Realty version has been a commercial application since version 3.0 was released. Today I am happy to announce, Open-Realty is back under the MIT open source license.  All features from the Pro version have been carried over to the open source software. We were only able to port the english language pack, but I hope to add more translations soon.
 
Check out the [change log for Open-Realty 3.4.0-beta.1](https://gitlab.com/appsbytherealryanbonham/open-realty/-/blob/main/CHANGELOG.md#340-beta1-2021-02-08) and start testing and contributing.

### Download Information

[Download Here](https://gitlab.com/appsbytherealryanbonham/open-realty/-/releases/3.4.0-beta.1)

Make sure to select the Package Download, as highlighted in this example screenshot. 

<img src="/assets/blog/release_download_screenshot.png" />
