---
title: Open-Realty 3.4.0 Released
date: 2021-02-21
description: Open-Realty 3.4.0 our first stable release under the new MIT license is available.
---

# Open-Realty 3.4.0 Released

Just 14 days after I announced beta 1 and our plans to open source Open-Realty, I am happy to announce the first stable release. Not only is Open-Realty now free and under an open source license, it has also been updated to work with PHP 7.4 and includes a number of other improvements and fixes. The Open-Realty website also has open source version of both TransparentRETS and TransparentMaps available in beta form, with stable releases coming soon.

Check out the [change log for Open-Realty 3.4.0 and the beta releases](https://gitlab.com/appsbytherealryanbonham/open-realty/-/blob/main/CHANGELOG.md#340-2021-02-21) and start upgrading.

### Download Information

[Download Here](https://gitlab.com/appsbytherealryanbonham/open-realty/-/releases/3.4.0)

Make sure to select the Package Download, as highlighted in this example screenshot.

<img src="/assets/blog/release_download_screenshot.png" />
