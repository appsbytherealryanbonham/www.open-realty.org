---
title: Open-Realty 3.5.4 Released
date: 2022-01-15
description: Open-Realty 3.5.4 - More security improvements and bug fixes.
---

Open-Realty 3.5.4 is our first bugfix release of the year. Along with bug fixes, this update contains some changes to help other developers get started developing for Open-Realty.

## Fixes

- Fixed Some E_NOTICE errors with Contact Form
- Fix some e_warning errors with the view lead pagination
- Fix an error in the login code for storing user data in sessions
- Fix warning errors that were breaking the ability to edit media in the media widget.
- Fix PHP error if you try to display a field with get_listing_single_value() that the user does not have access to.

## Other Changes

- Try to ensure we are running E_ALL error reporting if devignoreinstall is present
- Update Build images to use latest PHP 8 docker image with development-ini enabled.
- Fix makefile errors on windows and update README.md with windows development directions.
- Simplify Local Dev to run make commands in the container by default, to reduce the need for installing PHP, Node, Yarn all on the local machine.

# Download Information

- [Open-Realty Download Here](https://gitlab.com/appsbytherealryanbonham/open-realty/-/releases)

Make sure to select the Package Download, as highlighted in this example screenshot.

<img src="/assets/blog/release_download_screenshot.png" />
