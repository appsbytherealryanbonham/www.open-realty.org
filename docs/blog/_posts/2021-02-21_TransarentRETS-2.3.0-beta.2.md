---
title: TransparentRETS 2.3.0 Beta 2.
date: 2021-02-21
description: TransparentRETS 2.3.0 Beta 2 has been released..
---

# TransparentRETS goes Open Source

TransparentRETS has been the premier commercial addon for Open-Realty offering real estate agents the ability to import MLS data via RETS servers.

This release support forcing the use of https image links for remote photos. A useful feature for RETS server that return http links for remote photos, but support https as well.

Check out the [change log for TransparentRETS 2.3.0-beta.2](https://gitlab.com/appsbytherealryanbonham/transparentrets/-/blob/main/CHANGELOG.md#230-beta2-2021-02-21) and start testing and contributing.

### Download Information

[Download Here](https://gitlab.com/appsbytherealryanbonham/transparentrets/-/releases)

Make sure to select the Package Download, as highlighted in this example screenshot.

<img src="/assets/blog/release_download_screenshot.png" />
