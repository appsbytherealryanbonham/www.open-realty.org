---
title: Open-Realty 3.4.0-beta.3 Released
date: 2021-02-13
description:
   Open-Realty 3.4.0-beta.3 Released
---
 
# Open-Realty 3.4.0-beta.3 Released
 
Our third beta for 3.4.0 is now available. This third beta fixes an error discovered on new installs. This also updates the rss template to be compatible with more RSS readers. 

This version also now supports automated upgrades, going forward. 

Please note that Open-Realty now requires PHP 7.4, older PHP version will not work.
 
Check out the [change log for Open-Realty 3.4.0-beta.3](https://gitlab.com/appsbytherealryanbonham/open-realty/-/blob/main/CHANGELOG.md#340-beta3-2021-02-13) and start testing and contributing.

### Download Information

[Download Here](https://gitlab.com/appsbytherealryanbonham/open-realty/-/releases/3.4.0-beta.3)

Make sure to select the Package Download, as highlighted in this example screenshot. 

<img src="/assets/blog/release_download_screenshot.png" />
