---
title: Open-Realty 3.5.6 Released
date: 2022-02-02
description: Open-Realty 3.5.6 - Groundhog Day Bug Fixes
---

Open-Realty 3.5.6 contains a couple of bug fixes. .

## [3.5.5] - 2022-01-29

## [3.5.6] - 2022-02-02

### Fixed

- Missing `$misc` global variable in fields__value API calls. [#104](https://gitlab.com/appsbytherealryanbonham/open-realty/-/issues/104)
- Improved HTTPS protocol detection in installer when determining baseurl.
- Fixes Linking to files in Page Editor. [#105](https://gitlab.com/appsbytherealryanbonham/open-realty/-/issues/105)

### Other
- Autodocumented Makefile and set .PHONY
- Added Default issue template for Gitlab

# Download Information

- [Open-Realty Download Here](https://gitlab.com/appsbytherealryanbonham/open-realty/-/releases)

Make sure to select the Package Download, as highlighted in this example screenshot.

<img src="/assets/blog/release_download_screenshot.png" />
