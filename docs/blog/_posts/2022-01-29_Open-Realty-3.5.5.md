---
title: Open-Realty 3.5.5 Released
date: 2022-01-29
description: Open-Realty 3.5.5 - Security, Bug Fixes, Improved Developer Experience.
---

Open-Realty 3.5.5 is a security patch. Along with the security fixes, we have fixes some bugs and improved the developer experience.

## [3.5.5] - 2022-01-29

### Fixed

- SECURITY: Fixes possible SQL injection from improper use of addslashes()
- Fix redirect to installer
- Update PHP Dependencies
- Fixed PHP errors during media upload for vtours.
- Fixed display of field values for user information on the edit_user screen.
- Fix some misc PHP errors caught during code cleanup.

# Developer Changes

- Update Makefile to fix RUN_CMD detection on windows.
- More cross-platform improvements for Makefile & docker-compose.
- Support for running a development environment on Gitpod.io
- Cleaned up grammar in the CHANGELOG.md and README.md files.
- Enable the intelephense extension in gitpod.
- Reenable xdebug support in docker-compose.
- Added PHPMyAdmin to docker-compose setup.

# Download Information

- [Open-Realty Download Here](https://gitlab.com/appsbytherealryanbonham/open-realty/-/releases)

Make sure to select the Package Download, as highlighted in this example screenshot.

<img src="/assets/blog/release_download_screenshot.png" />
