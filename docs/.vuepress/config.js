const { description } = require('../../package')
import { defaultTheme } from '@vuepress/theme-default'
import { registerComponentsPlugin } from '@vuepress/plugin-register-components'
import { getDirname, path } from '@vuepress/utils'
import { usePagesPlugin } from 'vuepress-plugin-use-pages'
import { viteBundler } from 'vuepress'

const __dirname = getDirname(import.meta.url)

export default {
  /**
   * Ref：https://v1.vuepress.vuejs.org/config/#title
   */
  title: 'Open-Realty',
  /**
   * Ref：https://v1.vuepress.vuejs.org/config/#description
   */
  description: description,

  /**
   * Extra tags to be injected to the page HTML `<head>`
   *
   * ref：https://v1.vuepress.vuejs.org/config/#head
   */
  head: [
    ['meta', { name: 'theme-color', content: '#3eaf7c' }],
    ['meta', { name: 'apple-mobile-web-app-capable', content: 'yes' }],
    ['meta', { name: 'apple-mobile-web-app-status-bar-style', content: 'black' }]
  ],

  dest: 'public',
  /**
   * Theme configuration, here is the default theme configuration for VuePress.
   *
   * ref：https://v1.vuepress.vuejs.org/theme/default-theme-config.html
   */
  theme: defaultTheme({
    repo: '',
    editLinks: false,
    docsDir: 'docs',
    editLinkText: '',
    lastUpdated: false,
    colorMode: 'light',
    colorModeSwitch: false,
    logo: '/assets/img/logo.jpg',
    navbar: [

      {
        text: 'Features',
        link: 'features.html'
      },
      {
        text: 'Blog',
        link: '/blog/'
      },
      {
        text: 'Docs',
        link: 'https://docs.open-realty.org',
      },
      {
        text: 'Download',
        link: 'https://gitlab.com/appsbytherealryanbonham/open-realty/-/releases',
      },
      {
        text: 'Support (Discord)',
        link: 'https://discord.gg/kabJYuykTZ'
      }
    ],
    sidebar: {
      '/guide/': [
        {
          title: 'Guide',
          collapsable: false,
          children: [
            '',
            'using-vue',
          ]
        }
      ],
    },
  }),


  bundler: viteBundler(),
   /**
   * Apply plugins，ref：https://v1.vuepress.vuejs.org/zh/plugin/
   */
  plugins: [
    usePagesPlugin({
      startsWith: '/blog/_posts'
    }),
    registerComponentsPlugin({
      components: {
        BlogIndex: path.resolve(__dirname, './components/BlogIndex.vue'),
      },
    })
  ]
}
